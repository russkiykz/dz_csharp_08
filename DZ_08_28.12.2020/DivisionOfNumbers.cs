﻿using System;

namespace DZ_08_28._12._2020
{
    class DivisionOfNumbers
    {
        /// <summary>
        /// Метод деления одного числа на другое
        /// </summary>
        /// <param name="firstNumber"></param>
        /// <param name="secondNumber"></param>
        /// <returns>Результат деления двух чисел</returns>
        /// <exception cref="ArgumentException"/>
        /// <exception cref="ArgumentNullException"/>
        public double Division(string firstNumber, string secondNumber)
        {
            if (firstNumber=="" || secondNumber == "")
            {
                throw new ArgumentException("Неверные входящие параметры!");
            }
            else
            {
                double.TryParse(firstNumber, out double dividend);
                double.TryParse(secondNumber, out double divider);
                if (divider == 0)
                {
                    throw new ArgumentNullException("Делитель равен нулю!");
                }
                return dividend / divider;
            }
            
        }
    }
}
