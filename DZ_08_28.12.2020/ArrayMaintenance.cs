﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_08_28._12._2020
{
    public class ArrayMaintenance
    {
        /// <summary>
        /// Метод заполнения массива целых чисел случайными значениями
        /// </summary>
        /// <param name="array"></param>
        /// <param name="sizeArray"></param>
        /// <returns>Массив целых чисел</returns>
        public static int[] FillingArray(int[] array, int sizeArray)
        {
            Random random = new Random();
            for (int i = 0; i < sizeArray; i++)
            {
                array[i] = random.Next(0, 100);
            }
            return array;
        }

        /// <summary>
        /// Метод вывода на консоль массива целых чисел
        /// </summary>
        /// <param name="array"></param>
        /// <param name="sizeArray"></param>
        public static void PrintArray(int[] array, int sizeArray)
        {
            Console.Write("\nМассив: ");
            for (int i = 0; i <= sizeArray; i++)
            {
                Console.Write($"{array[i]} ");
            }
            Console.WriteLine();
        }
    }
}
