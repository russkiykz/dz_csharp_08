﻿using System;

namespace DZ_08_28._12._2020
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.
            Console.Write("Введите делимое: ");
            string dividend = Console.ReadLine();
            Console.Write("Введите делитель: ");
            string divider = Console.ReadLine();
            var division = new DivisionOfNumbers();
            try
            {
                Console.WriteLine($"\nРезультат: {division.Division(dividend, divider)}\n");
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine($"\n{exception.Message}\n");
            }

            Console.WriteLine("-----------------");

            // 2.
            Console.Write("Введите размер массива: ");
            int.TryParse(Console.ReadLine(), out int sizeArray);
            int[] arrayRandomOfNumbers = new int[sizeArray];
            ArrayMaintenance.FillingArray(arrayRandomOfNumbers, sizeArray);
            try
            {
                ArrayMaintenance.PrintArray(arrayRandomOfNumbers, sizeArray);
            }
            catch(Exception exception)
            {
                Console.WriteLine($"\n\n{exception.Message}");
            }
            finally
            {
                Console.WriteLine("\nОбработка массива завершена!");
            }
        }
    }
}
